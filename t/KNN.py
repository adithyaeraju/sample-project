from numpy import genfromtxt
import IPython
import numpy as np
test_data = genfromtxt('mnist_te.csv', delimiter = ',')
train_data = genfromtxt('mnist_tr.csv', delimiter = ',')
test_shape = test_data.shape
testlimit = int(test_shape[0])
train_shape = train_data.shape
trainlimit = int(train_shape[0])
x_train_new = train_data[:trainlimit,1:]
y_train_new = train_data[:trainlimit,0]
x_test_new = test_data[:testlimit,1:]
y_test_new = train_data[:testlimit,0]
k = [1,9,19,29,39,49,59,69,79,89,99]
#IPython.embed()
k_shape = len(k)
acc= np.zeros(k_shape)
for index,i in enumerate(x_test_new):
	distance=[]
	for ks in range (0,k_shape):
		dist_k = []
    		if (ks==0): 
      			for j in x_train_new:
              			j = j - i
        			j = np.square(j)
        			t = np.sum(j)
        			t = np.sqrt(t)
        			distance.append(t)
   			distance = np.vstack((distance,y_train_new)).T
     			distance = sorted(distance,key=lambda distance:distance[0])
     		for l in range (0,k[ks]):
         		lab = distance [l][1]
         		dist_k.append(lab)
        	(value,count) = np.unique(dist_k,return_counts = True)
         	freq_index = np.argmax(count)
         	max_lab = value[freq_index]
         	if(max_lab == y_test_new[index]):
         		acc[ks] = acc[ks]+1.0
for ks in range (0,k_shape):
   acc[ks] = acc[ks]/testlimit
print acc
