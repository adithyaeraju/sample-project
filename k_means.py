import numpy as np
import matplotlib.pyplot as plt
import IPython
import random
import matplotlib.pyplot as plt
data_set = np.genfromtxt('data_set.csv', delimiter = ',')
K = [2,3,4,5,6,7,8]
feature_set = data_set[:,1:10]
potential_function=[]

def centroids(feature_set,k):
    rand = []
    k_centroid=[]
    for j in range(0,k):
        x=random.randint(0,699)
        centroid = feature_set[x][:]
        k_centroid.append(centroid)
    return k_centroid

def distance(feature_set,k_centroid,k):
    cluster = np.ones((699,))
    distance = np.sqrt(np.sum(np.square(np.subtract(feature_set,k_centroid[0][:])),axis=1))
    for i in range(1,k):
        for index,j in enumerate(feature_set):
            if ((np.sqrt(np.sum(np.square(np.subtract(j,k_centroid[i][:])))))<distance[index]):
                distance[index]=np.sqrt(np.sum(np.square(np.subtract(j,k_centroid[i][:]))))
                cluster[index]=i+1
    return cluster

def new_centroid(cluster,feature_set,k_centroid, kvalue):
    new_cent = []
    empty=np.zeros((9))
    for i in range(0,kvalue):
        centroid=np.zeros((9))
        count = 0.0
        for index,j in enumerate(cluster):
            if (j== i+1):
                centroid = np.add(centroid,feature_set[index])
                count+=1.0
        if(count>0.0):
            centroid=np.divide(centroid,count)
            new_cent.append(centroid)
        else:
            new_cent.append(empty)
    return new_cent

def pot_fn(feature_set,centroid,cluster,kvalue):
    potential_function=0.0
    for index,i in enumerate(cluster):
        potential_function = potential_function + np.sum(np.square(np.subtract(feature_set[index],centroid[int(i-1.0)][:])))
    return potential_function


for kvalue in K:
    i=True
    k_centroid = centroids(feature_set,kvalue)
    if i == True:
        cluster = distance(feature_set,k_centroid,kvalue)
        newcentroid = new_centroid(cluster,feature_set,k_centroid,kvalue)
        if (np.sum(np.square(np.subtract(newcentroid,k_centroid)))== 0.0):
            i = False
        else:
            k_centroid=newcentroid
    potential_function.append(pot_fn(feature_set,newcentroid,cluster,kvalue))
print potential_function
plt.plot(K,potential_function)
plt.xlabel('Kvalue')
plt.ylabel('potential Function')
plt.show()
